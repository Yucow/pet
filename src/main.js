import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//UI框架
import ElementUI from 'element-ui';
Vue.use(ElementUI);
import 'element-ui/lib/theme-chalk/index.css';
//重置样式
import '@/assets/css/reset.scss';
//动画特效
import animated from 'animate.css'
Vue.use(animated)

//工具
import '@/utils/mixin.js';
import '@/utils/driver.js';

// excel导出
import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#root')
