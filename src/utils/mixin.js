import Vue from 'vue'

Vue.mixin({
    methods: {
        jump (url) {
            this.$router.replace({ path: url })
        }
    },
})