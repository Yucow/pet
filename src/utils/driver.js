export default [{
        element: '#some-element1',
        popover: {
            title: '后台首页',
            // description: '第一项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element2',
        popover: {
            title: '商品分类',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element3',
        popover: {
            title: '商品属性',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element4',
        popover: {
            title: '商品管理',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element5',
        popover: {
            title: '订单管理',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element6',
        popover: {
            title: '用户管理',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element7',
        popover: {
            title: '角色管理',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element8',
        popover: {
            title: '权限管理',
            // description: '第二项的描述',
            position: 'right',
        }
    },
    {
        element: '#some-element9',
        popover: {
            title: '导航收缩',
            // description: '第二项的描述',
            position: 'bottom',
        }
    },
]