import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            // 登录页
            path: '/login', name: 'login', component: () => import('../views/admin/Login.vue'),
        },
        {
            path: '/index', alias: '/', component: () => import('../views/admin/Index.vue'),
            children: [
                //店铺配置
                { path: '/deploy/clerk', component: () => import('../views/deploy/Clerk.vue'), },
                { path: '/deploy/message', component: () => import('../views/deploy/Message.vue'), },
                { path: '/deploy/recycle', component: () => import('../views/deploy/Recycle.vue'), },
                { path: '/deploy/store', component: () => import('../views/deploy/Store.vue'), },
                { path: '/deploy/system', component: () => import('../views/deploy/System.vue'), },
                //数据统计
                { path: '/data/business', component: () => import('../views/data/Business.vue') },
                { path: '/data/sale', component: () => import('../views/data/Sale.vue') },
                { path: '/data/userreport', component: () => import('../views/data/Userreport.vue') },
                //店面业务
                { path: '/business/orders', name: 'index', component: () => import('../views/business/Orders.vue'), },
                { path: '/business/send', name: 'index', component: () => import('../views/business/Send.vue'), },
                { path: '/business/account', name: 'index', component: () => import('../views/business/Account.vue'), },
                { path: '/business/money', name: 'index', component: () => import('../views/business/Money.vue'), },
                // 商品服务
                { path: '/serve/goods', component: () => import('../views/serve/Goods.vue') },
                { path: '/serve/serve', component: () => import('../views/serve/Serve.vue') },
                { path: '/serve/living', component: () => import('../views/serve/Living.vue') },
                // 库存管理
                { path: '/store/goods', name: 'index', component: () => import('../views/store/Goods.vue') },
                { path: '/store/record', name: 'index', component: () => import('../views/store/Record.vue') },
                { path: '/store/details', name: 'index', component: () => import('../views/store/Details.vue') },
                { path: '/store/enterrecord', name: 'index', component: () => import('../views/store/Enterrecord.vue') },
                //会员中心
                { path: '/member/manage', component: () => import('../views/member/Manage.vue'), },
                { path: '/member/card', component: () => import('../views/member/Card.vue'), },
                { path: '/member/times', component: () => import('../views/member/Times.vue'), },
                //survey
                { path: '/survey', component: () => import('../views/admin/Survey.vue') },

            ],
        }]
})

export default router
